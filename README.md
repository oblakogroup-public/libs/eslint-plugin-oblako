# eslint-plugin-oblako

Pack of rules for oblakogroup

## Installation

You'll first need to install [ESLint](http://eslint.org):

```
$ npm i eslint --save-dev
```

Next, install `eslint-plugin-oblako`:

```
$ npm install eslint-plugin-oblako --save-dev
```


## Usage

Add `oblako` to the plugins section of your `.eslintrc` configuration file. You can omit the `eslint-plugin-` prefix:

```json
{
    "plugins": [
        "oblako"
    ]
}
```


Then configure the rules you want to use under the rules section.

```json
{
    "rules": {
        "@oblakogroup-public/lint/no-map-without-usage": "error",
        "@oblakogroup-public/lint/prefer-arrow": "error"
    }
}
```




