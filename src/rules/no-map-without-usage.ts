import { AST_NODE_TYPES, TSESTree } from '@typescript-eslint/experimental-utils';
import * as utils from '../utils';

type Options = [];
type MessageIds = 'message';

export default utils.createRule<Options, MessageIds>({
  meta: {
    type: 'suggestion',
    messages: {
      message: 'Return value from Array.prototype.map should be assigned to a variable. ' +
        'Consider using Array.prototype.forEach instead.',
    },
    schema: [],
  },
  create(context) {
    return {
      'ExpressionStatement > CallExpression > MemberExpression > Identifier[name = "map"].property'(
        node: TSESTree.Identifier
      ): void {
        const parents_type = node.parent?.parent?.parent?.type;

        if (parents_type && parents_type === AST_NODE_TYPES.ExpressionStatement) {
          context.report({
            messageId: 'message',
            node,
          });
        }
      }
    }
  },
});

export { MessageIds, Options };
