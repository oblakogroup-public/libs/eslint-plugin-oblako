import { TSESTree } from '@typescript-eslint/experimental-utils';
import * as utils from '../utils';

type Options = [];
type MessageIds = 'message';

export default utils.createRule<Options, MessageIds>({
  meta: {
    type: 'suggestion',
    messages: {
      message: 'Prefer using arrow functions over plain functions which only return a value.',
    },
    fixable: 'code',
    schema: [],
  },
  create(context) {
    return {
      'ArrowFunctionExpression > BlockStatement > ReturnStatement'(
        node: TSESTree.ReturnStatement,
      ): void {
        const block = node.parent as TSESTree.BlockStatement;

        if (block.body.length === 1) {
          const nextText = node.argument !== null ? context.getSourceCode().getText(node.argument) : '{}';

          context.report({
            messageId: 'message',
            node,
            fix: fixer => fixer.replaceText(block, nextText),
          });
        }
      },
    }
  },
});

export { MessageIds, Options };
