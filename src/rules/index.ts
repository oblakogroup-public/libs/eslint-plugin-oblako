import noMapWithoutUsage from './no-map-without-usage';
import preferArrow from './prefer-arrow';

export default {
  'no-map-without-usage': noMapWithoutUsage,
  'prefer-arrow': preferArrow,
};
