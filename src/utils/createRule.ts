import { TSESLint } from '@typescript-eslint/experimental-utils';

function createRule<
  TOptions extends readonly unknown[],
  TMessageIds extends string,
  TRuleListener extends TSESLint.RuleListener = TSESLint.RuleListener
>({ meta, create }: Readonly<{
  meta: TSESLint.RuleMetaData<TMessageIds>;
  create: (
    context: Readonly<TSESLint.RuleContext<TMessageIds, TOptions>>,
  ) => TRuleListener;
}>): TSESLint.RuleModule<TMessageIds, TOptions, TRuleListener> {
  return {
    meta,
    create(
      context: Readonly<TSESLint.RuleContext<TMessageIds, TOptions>>,
    ): TRuleListener {
      return create(context);
    },
  };
}

export { createRule };
