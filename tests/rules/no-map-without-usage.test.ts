import rule from '../../src/rules/no-map-without-usage';
import { ESLintUtils } from '@typescript-eslint/experimental-utils';

const ruleTester = new ESLintUtils.RuleTester({
  parser: '@typescript-eslint/parser',
});

ruleTester.run('no-map-without-usage', rule, {
  valid: [
    '[].map(() => 0).forEach(() => ({}));',
    'const a = [].map(() => 0);',
    'const b = [].map(() => 0).forEach(() => ({}));',
  ],
  invalid: [
    {
      code: '[].map(() => 0);',
      errors: [{ messageId: 'message' }],
    }
  ],
});
