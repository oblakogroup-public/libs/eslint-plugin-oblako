import rule from '../../src/rules/prefer-arrow';
import { ESLintUtils } from '@typescript-eslint/experimental-utils';

const ruleTester = new ESLintUtils.RuleTester({
  parser: '@typescript-eslint/parser',
});

ruleTester.run('no-map-without-usage', rule, {
  valid: [
    '[].forEach(() => ({}));',
    '[].map(() => 0);',
    '[].map(() => 0).forEach(() => ({}));',
    '[].map(() => { var a = 0; return a; });', // cuz variable in
  ],
  invalid: [
    {
      code: '[].map(() => { return 0; });',
      output: '[].map(() => 0);',
      errors: [{ messageId: 'message' }],
    },
    {
      code: '[].map((a) => { return a; });',
      output: '[].map((a) => a);',
      errors: [{ messageId: 'message' }],
    },
    {
      code: '[].map((a, _b) => { return a; });',
      output: '[].map((a, _b) => a);',
      errors: [{ messageId: 'message' }],
    },
    {
      code: '[].map(() => { return; });',
      output: '[].map(() => {});',
      errors: [{ messageId: 'message' }],
    },
    {
      code: '[].map((a) => { return; });',
      output: '[].map((a) => {});',
      errors: [{ messageId: 'message' }],
    },
  ],
});
