import plugin from '../src';
import rules from '../src/rules';

describe('export', () => {
  it('correct export', () => {
    expect(Object.keys(rules)).toEqual(expect.arrayContaining(Object.keys(plugin.rules)));
  });
});
